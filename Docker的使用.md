# Docker学习笔记

____

## 简介

Docker 是一个开源的应用容器引擎，让开发者可以打包他们的应用以及依赖包到一个可移植的[镜像](https://baike.baidu.com/item/%E9%95%9C%E5%83%8F/1574?fromModule=lemma_inlink)中, 借助它, 可以方便的部署,配置,管理我们的应用程序

## 优势

- 标准化: Docker 提供了可复用的开发、构建、测试和生产环境, 解决了开发环境搭建的问题

- 弹性计算: 根据需要，动态地添加新的应用服务，在不需要时收回服务器资源

- 良好的兼容性和可移植性

- 配置快速简单

- Docker 能够确保应用和资源被充分隔离

## 常用命令

- 查看所有的容器: `docker ps -a`

- 进入容器: `docker exec -it nginx /bin/bash`

- 退出容器: `exit`

- 查看容器的进程: `docker top nginx`

- 重建某个容器: `docker-compose -f docker-compose-env.yml up -d --force-recreate --build kibana`

- 查看容器资源占用: `docker stats`

- 启动项目: `docker-compose up -d`(需要在项目根目录下执行此命令)

### 镜像常用命令

```
docker pull [镜像名称:版本] 拉取镜像
docker images  镜像列表
docker rmi [镜像名称:版本] 删除镜像
docker history [镜像名称] 镜像操作记录
docker tag [镜像名称:版本][新镜像名称:新版本]
docker inspect [镜像名称:版本] 查看镜像详细
docker search [关键字] 搜索镜像
docker login 镜像登陆
```

### 容器常用命令

```
docker ps -a 容器列表(所有容器)
docker ps  查看所有(运行的)容器
docker exec -ti <id> bash  以 bash 命令进入容器内
docker run -ti --name [容器名称][镜像名称:版本] bash 启动容器并进入
docker logs 查看容器日志
docker top <container_id> 查看容器最近的一个进程
docker run -ti --name [容器名称] -p 8080:80 [镜像名称:版本] bash  端口映射
docker rm <container_id> 删除容器
docker stop <container_id> 停止容器
docker start <container_id> 开启容器
docker restart <container_id> 重启容器
docker inspect <container_id> 查看容器详情
docker commit [容器名称] my_image:v1.0  容器提交为新的镜像
```
