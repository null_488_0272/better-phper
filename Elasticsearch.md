# Elasticsearch学习笔记

____

## 简介

Elasticsearch 是一个分布式实时分析搜索引擎, 使用RESTful API进行通信，并使用无模式的JSON文档存储数据。它基于Java编程语言构建，因此Elasticsearch可以在不同平台上运行

## 优势

- 支持大量、离散、关键词式的查询

- 迁移、扩容很简单

- 符合日志系统的需求

- Elasticsearch是在Java上开发的，这使得它在几乎所有平台上都兼容

- Elasticsearch是分布式的，因此可以轻松地进行扩展和集成

- Elasticsearch使用JSON对象作为响应，这使得可以使用大量不同的编程语言来调用Elasticsearch服务器

## 关键概念

- 节点 : 它指的是Elasticsearch的单个运行实例。单个物理和虚拟服务器可容纳多个节点

- 集群 : 它是一个或多个节点的集合。群集为所有数据提供了跨所有节点的集体索引和搜索功能

- 索引 : 它是不同类型的文档及其属性的集合。索引还使用分片的概念来提高性能

- 文档 : 它是以JSON格式定义的字段集合。每个文档都属于一种类型，并且位于索引内。每个文档都与一个称为UID的唯一标识符相关联

- 副本 : Elasticsearch允许用户创建索引和碎片的副本。副本不仅有助于在发生故障时提高数据的可用性，而且还通过在这些副本中执行并行搜索操作来提高搜索性能 

- 分片 : 因为Elasticsearch是一个分布式搜索引擎，索引通常会拆分为分布在多个节点上的称为分片的元素。Elasticsearch自动管理这些分片的排列。它还根据需要重新平衡分片

- Elasticsearch默认端口`9200`

- 在 Elasticsearch 中，我们对文档进行索引、检索、排序和过滤—​而不是对行列数据

- 在 Elasticsearch 中为什么使用JSON作为文档的序列化格式, 因为JSON 序列化为大多数编程语言所支持, 它简单、简洁、易于阅读

- 一个 Elasticsearch 集群可以 包含多个`索引 `，相应的每个索引可以包含多个`类型` 。 这些不同的类型存储着多个`文档`，每个文档又有多个`属性`

- Elasticsearch中的一个索引类似于传统关系数据库中的一个`数据库`

- Elasticsearch使用倒排索引

- 在Elasticsearch中, 路径`/iclock/employee/100`包含三部分信息, `iclock`(索引名称), `employee` (类型名称), `100` (文档ID)

- Elasticsearch支持`查询字符串`和`查询表达式`两种搜索方式

- Elasticsearch 默认按照相关性得分排序，即每个文档跟查询的匹配程度

- Elasticsearch采用`乐观并发控制`, 当有多个请求对同一个文档进行修改时, 可以通过指定文档的`版本号`来避免冲突

- `过滤查询`与`评分查询`,当使用`过滤查询`时不会对结果集的匹配程度进行评分, 当使用`评分查询`时会需要根据文档的匹配程度进行评分,并按照相关性对匹配到的文档进行排序

- `过滤查询`只是简单的检索, 计算起来非常快, 结果会被缓存到内存中以便快速读取，所以有各种各样的手段来优化查询结果

- `评分查询`不仅仅要找出匹配的文档，还要计算每个匹配文档的相关性，计算相关性使得它们比不评分查询费力的多。同时，查询结果并不缓存

- 如何选择`评分查询`与`过滤查询`, 通常的规则是，使用`评分查询`语句来进行全文搜索或者其它任何需要影响相关性得分的搜索。除此以外的情况都使用过滤查询

- Elasticsearch中Text 与. keyword字段类型的区别
  
  ```
  Text：会分词，然后进行索引、支持模糊查询、精确查询,不支持聚合
  keyword：不进行分词，直接索引、支持模糊查询、精确查询,支持聚合
  ```

## 关键字的作用说明

- `match`:  模糊匹配,类似于mysql的like
- `multi_match`:  查询多个字段, 也可以使用_all关键字达到相同的效果
- `match_phrase`:  短语匹配,匹配一个完整的句子
- `match_phrase_prefix`:  前缀短语匹配, 可实现输入框即时搜索功能
- `term`:  精确匹配,比如查询指定日期的文档,类似于where field = ?
- `terms`:   精确匹配多个值,类似于where in
- `range`:   范围匹配,比如查询日期范围内的文档
- `must`:   所有的语句都必须匹配，与 AND 等价
- `must_not`:  所有的语句都 不能匹配，与 NOT 等价
- `should`:  至少有一个语句要匹配，与 OR 等价
- `bool`:  表示开启一个bool过滤器
- `minimum_should_match`:  指定shoule查询条件的匹配度,可设置为整数或者百分数
- `operator`:  当使用match进行多值匹配时, operator可以指定是进行and还是or匹配,minimum_should_match也能实现类似效果
- `_all`:  表示文档中的所有字段, 如果需要对文档中的所有字段进行匹配,可以指定__all作为查询字段
- `fields`:  指定查询字段
- `slop`:  通过它可实现近似匹配功能, 如搜索"贵州省万山区"能够匹配到"贵州省铜仁市万山区"这样的文档
- `max_expansions`:  可以通过设置 max_expansions 参数来限制前缀扩展的影响
- `constant_score`:  指定以非评分模式来执行查询
- `filter`:  查询置于 filter 语句内不进行评分或相关度的计算
- `exists`:  存在查询, 比如查询包含某个属性的文档
- `_source`:  限定文档返回的字段,类似于mysql中select的作用
- `boost`:  在组合查询中提升子句的查询权重
- `keyword`: 指定被搜索字段以.keyword类型进行匹配(.keyword类型不进行分词，直接索引  支持模糊、精确查询  支持聚合)
- `raw`: 指定聚合字段不进行分词处理

### linux查询命令

```
curl -XPOST -H 'Content-Type: application/json' http://127.0.0.1:9200/books/_search?pretty -d '{"query":{"match":{"name":"美人"}}}'
```

## 使用案例

- multi_match的使用, 查询title和content包含中国的文档
  
  ```
  POST /chinese/_search
  {
    "query": {
       "multi_match": {
          "query": "中国",
          "type": "most_fields",
          "operator": "and",
          "fields": [ "title", "content" ]
       }
     }
  }
  ```

- 范围查询, 查询年龄在20至30岁之间的员工 
  
  ```
  POST /iclock/_search
  {
    "query": {
       "range": {
          "age": {
              "gte":  20,
              "lt":   30
          }
       }
     }
  }
  ```

- 测试指定分词器
  
  ```
  POST _analyze
  {
    "analyzer": "standard",
    "text": "The quick brown fox"
  }
  ```

- 查询兴趣爱好为music和sports的人, interests是一个数组类型的字段
  
  ```
  //operator选项控制以and还是or的方式匹配music,forestry关键字
  //minimum_should_match选项可以百分比或数字的方式来控制匹配
  POST /iclock/_search
  {
     "query": {
       "match": {
         "interests": "music forestry",
         "operator": "and"
       }
     }
  }
  ```

- 短语匹配, 如果使用match的话只要包含中国或者韩国的记录都会被匹配, 使用match_phrase的话要求所有的分词必须同时出现在文档中，同时位置必须紧邻一致
  
  ```
  {
     "query": {
        "match_phrase": {
          "about": "中国韩国"
        }
      }
   }
  ```

- 更新文档, "PCn-ToYBWEPklukOWUgI"是文档的__id
  
  ```
  POST /hero/_doc/PCn-ToYBWEPklukOWUgI/_update
  {
   "doc":{
      "address":"贵州省铜仁市万山区黄道乡"
     }
  }
  ```

- 使用_all关键字查询所有字段
  
  ```
  POST /hero/_search
  {
    "query": {
       "match": {
          "_all": "john smith marketing"
        }
     }
  }
  ```

- 实现即时搜索功能
  
  ```
  POST /hero/_search
  {
     "query":{
          "match_phrase_prefix" : {
            "brand": "johnnie walker bl"
         }
      }
  }
  
  POST /hero/_search
  {
    "query": {
       "match_phrase_prefix" : {
          "brand": {
             "query": "johnnie walker bl",
             "max_expansions": 50,//限制前缀的扩展, 当匹配结果集过多时可能产生性能问题,需要设置该值
             "slop": 5 //实现近似匹配需要设置该值
           }
        }
     }
  }
  ```

- 实现近似匹配功能, 下列的查询可以匹配address为"贵州省铜仁市万山区"这样的记录
  
  ```
  POST /hero/_search
  {
    "query": {
      "match_phrase": {
        "address": {
          "query": "贵州省万山区",
          "slop": 5
         }
       }
     }
  }
  ```

- 布尔过滤器的使用
  
  ```
  POST /hero/_search
  {
   "query": {
      "bool": {
        "must": [],
        "should": [],
        "must_not": [],
        "minimum_should_match": 1//指定should的中的条件至少满足一个
      }
    }
  }
  ```

- 组合查询
  
  ```
  GET /_search
  {
      "query": {
          "bool": {
              "must": {
                  "match": {  
                      "content": {
                          "query": "full text search",
                          "operator": "and"
                      }
                  }
              },
              "should": [
                  { "match": {
                      "content": {
                          "query": "Elasticsearch",
                          "boost": 3 
                      }
                  }},
                  { "match": {
                      "content": {
                          "query": "Lucene",
                          "boost": 2 
                      }
                  }}
              ]
          }
      }
  }
  ```

- 提升单个字段的权重(可以使用 `^` 字符语法为单个字段提升权重，在字段名称的末尾添加 `^boost` ，其中 `boost` 是一个浮点数)
  
  ```
  {
      "multi_match": {
          "query":  "Quick brown fox",
          "fields": ["name^2", "title^3", "desc"] 
      }
  }
  ```

- _source关键字的使用
  
  ```
  POST /phone/_search
   {
     "_source": ["name", "desc", "price"], 
     "query": {
        "match_phrase": {
          "name": "苹果"
        }
      }
   }
  ```

- keyword关键字的使用
  
  ```
  POST /phone/_search
  {
     "_source": ["name", "desc", "price"], 
     "query": {
        "term": {
          "properties.value.keyword": "白色"
        }
      }
  }
  ```

- 聚合时keyword关键字的使用
  
  ```
  POST /phone/_search
  {
     "size": 0, 
     "aggs": {
        "popular_colors": {
          "terms": {
            "field": "brand.keyword",
            "size": 5
          }
        }
      }
  }
  ```
