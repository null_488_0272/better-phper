- `vmstat` 命令用来观察服务器详细状态, 可使用`dstat`命令代替`vmstat`  能输出比 `vmstat` 更加：美观，整洁，强大的内容 
  
  ```
  选项与参数：
  -a ：使用 inactive/active（活跃与否） 取代 buffer/cache 的内存输出信息；
  -f ：开机到目前为止，系统复制 （fork） 的程序数；
  -s ：将一些事件 （开机至目前为止） 导致的内存变化情况列表说明；
  -S ：后面可以接单位，让显示的数据有单位。例如 K/M 取代 Bytes 的容量；
  -d ：列出磁盘的读写总量统计表
  -p ：后面列出分区，可显示该分区的读写总量统计表
  指标说明:
  (procs)  r：等待运行中的程序数量；b：不可被唤醒的程序数量。这两个项目越多，代表系统越忙碌
  (memory) swpd：虚拟内存被使用的容量； free：未被使用的内存容量； buff：用于缓冲内存； cache：用于高速缓存内存
  (swap)   si：由磁盘中将程序取出的量, so：由于内存不足而将没用到的程序写入到磁盘的swap的容量
  (io)     bi：由磁盘读入的区块数量； bo：写入到磁盘去的区块数量。如果这部份的值越高，代表系统的 I/O 非常忙碌
  (system) in：每秒被中断的程序次数； cs：每秒钟进行的事件切换次数；这两个数值越大，代表系统与周边设备的沟通非常频繁！ 
  ```

- `iostat`查询磁盘IO情况
  
  ```
  使用示例:
  iostat [-d] [-k] [device] [interval] [count]
  选项与参数：
  -d       :只显示磁盘I/O的统计信息, 而不是默认信息, 默认信息中包含CPU使用情况
  -k         :按KB显示统计信息, 而不是按块显示    
  -x         :显示扩展性能I/O统计信息    
  device   :若指定设备,则只显示该设备信息
  interval :采样间隔时间
  count    :获取样本总数
  统计信息:
  tps          #每秒传输次数,该项为每秒对设备/分区读写的请求次数    
  kb_read/s #每秒读取磁盘的数据量，单位为K
  kb_wrtn/s #每秒写入磁盘的数据量，单位为K
  kb_read   #在时间间隔内读取磁盘的数据总量
  kb_wrtn   #在时间间隔内写入磁盘的数据总量
  Device    #监测设备名称
  rrqm/s    #每秒需要读取需求的数量
  wrqm/s    #每秒需要写入需求的数量
  r/s       #每秒实际读取需求的数量
  w/s       #每秒实际写入需求的数量
  rsec/s    #每秒读取区段的数量
  wsec/s    #每秒写入区段的数量
  rkB/s     #每秒实际读取的大小，单位为KB
  wkB/s     #每秒实际写入的大小，单位为KB
  avgrq-sz  #需求的平均大小区段
  avgqu-sz  #需求的平均队列长度
  await     #等待I/O平均的时间（milliseconds）
  svctm     #I/O需求完成的平均时间
  %util     #被I/O需求消耗的CPU百分比
  ```

- `uptime`  查看系统瓶颈负载`load average`就表示系统最近 `1 分钟、5 分钟、15 分钟`的系统瓶颈负载。 

- `lscpu` 查看 CPU 信息 

- `cat /proc/cpuinfo`查看每个 `CPU` 核的信息 

- `stress`是一个施加系统压力和压力测试系统的工具，我们可以使用`stress`工具压测试 `CPU`，以便方便我们定位和排查 `CPU` 问题 

- `mpstat`使用`mpstat -P ALL 1`则可以查看每一秒的 CPU 每一核变化信息，整体和`top`类似，好处是可以把每一秒（自定义）的数据输出方便观察数据的变化，最终输出平均数据 

- `pidstat` 用于查看全部或指定进程的`cpu`、内存、线程、设备`IO`等系统资源的占用情况 

- ```
  pidstat -urd -h -p 18225 5 (-urd)查看cpu,内存,io三个指标, (-h)内存，cpu 、io的信息合并在一栏
  ```

- `pidof` 查找指定名称的进程的进程号

- `cat /etc/passwd` 查看系统所有用户  

- `cat /etc/group` 查看系统所有组 

- `lsof` 查看进程打开的文件,  参数 `-p + 进程号`(查看某个进程打开的文件) `lsof | grep mysql` 查看`mysql`打开的文件  

- `free`  查看系统内存使用情况 , `free 参数 -b(以Bety为单位) -k(以kb为单位) -m(MB为单位) -g(以GB为单位) 展示`  -s<间隔秒>持续观察内存使用情况  -h ：以人们较易阅读的 `GBytes, MBytes, KBytes` 等格式自行显示

- `df`  查看磁盘使用情况 

- 文件压缩与解压缩命令
  
  ```
  tar -zcf test.tar.gz test.txt 将文件压缩
  tar -cf all.tar *.jpg 这条命令是将所有.jpg的文件打成一个名为all.tar的包。-c表示产生新的包，-f指定包的文件名。
  tar -rf all.tar *.gif 这条命令是将所有.gif的文件增加到all.tar的包里面去，-r表示增加文件的意思。 
  ```

- 如果希望执行某个命令，但又不希望在屏幕上显示输出结果，那么可以将输出重定向到 `/dev/null`： 
  
  ```
  $ command > /dev/null
  ```

- `/dev/null` 是一个特殊的文件，写入到它的内容都会被丢弃；如果尝试从该文件读取内容，那么什么也读不到。但是 `/dev/null` 文件非常有用，将命令的输出重定向到它，会起到"禁止输出"的效果。 如果希望屏蔽 `stdout` 和 `stderr`，可以这样写 
  
  ```
  $ command > /dev/null 2>&1
  ```

- `netstat -pnltu` 查看服务及它们所监听的端口

- 下例命令解释: 查找/var/log目录中更改时间在7日以前的普通文件，并删除它们 
  
  ```
  find /var/log -type f -mtime +7 -exec rm {} \;
  ```

- `ls -lht`   查看当前文件夹下各文件夹大小 

- 查看当前文件夹下各文件夹大小  
  
  ```
  du -h --max-depth=1
  du -sh `ls`
  du -sh /* 递归查看
  ```

- 环境变量设置 
  
  ```
  echo $PATH  查看环境变量
  env  查看环境变量
  vim /etc/profile  编辑环境变量
  vim .bash_profile 编辑环境变量
  ```

- 磁盘管理 
  
  ```
  1: fdisk 磁盘管理命令通过这个命令来添加分区和分配分区的大小
  2: mkfs  通过这个命令来格式化分区后分区才能使用
  3: mount 使用这个命令将分区挂载到指定的文件夹上
  4: vim /etc/fstab 将挂载信息永久记录,使用mount命令挂载的分区服务器重启后会失效,所以需要在
  /etc/fstab配置文件中记录挂载信息使其永久有效
  ```

- `lsblk -f` 查看系统分区情况 

- 通过`cat`命令快捷创建文件并输入内容 
  
  ```
  1. cat > test.php
  2. 输入内容
  3. Ctrl + d 保存
  ```

- 服务器使用 `CDN` 访问静态资源（图片、`JS`、`CSS` 文件）的目的是,`CDN`资源会分发到各个地区,当客户端访问这些资源时,可采用就近访问的原则降低访问这些资源的网络延迟, 另一个好处是减轻服务器带宽负载

- 查看指定端口号的占用情况`lsof -i:端口号`

- 查看本机公网IP `curl ifconf.me`

- 批量查找 `find *.log |xargs grep 合同`

- `ps -auxf`和`ps -axjf`以程序树的方式展示系统进程

- `ps -aux`命令的各项指标含义
  
  ```
  USER    : 该process属于那个使用者帐号的？
  PID     : 该process属于那个使用者帐号的？
  %CPU`   : 该process使用掉的 CPU 资源百分比
  %MEM    : 该process所占用的实体内存百分比 
  VSZ     : 该process使用掉的虚拟内存量 （KBytes）
  RSS     : 该process占用的固定的内存量 （KBytes）
  TTY     : 该process是在那个终端机上面运行
  STAT    : 该process目前的状态  
  START   : 该process被触发启动的时间；
  TIME    : 该process实际使用 CPU 运行的时间。
  COMMAND : 该process的实际指令为何？
  ```

- `top`命令参数说明
  
  ```
  -d ：后面可以接秒数，就是整个程序画面更新的秒数。默认是 5 秒；
  -b ：以批次的方式执行 top ，还有更多的参数可以使用喔！通常会搭配数据流重导向来将批次的结果输出成为文件。
  -n ：与 -b 搭配，意义是，需要进行几次 top 的输出结果。
  -p ：指定某些个 PID 来进行观察监测而已。
  ```

- `top` 执行过程当中可以使用的按键指令
  
  ```
  ? ：显示在 top 当中可以输入的按键指令；
  P ：以 CPU 的使用资源排序显示；
  M ：以 Memory 的使用资源排序显示；
  N ：以 PID 来排序喔！
  T ：由该 Process 使用的 CPU 时间累积 （TIME+） 排序。
  k ：给予某个 PID 一个讯号 （signal）
  r ：给予某个 PID 重新制订一个 nice 值。
  q ：离开 top 软件的按键。
  ```

- `pstree`查看进程的关联性
  
  ```
  -A ：各程序树之间的连接以 ASCII 字符来连接；
  -U ：各程序树之间的连接以万国码的字符来连接。在某些终端接口下可能会有错误；
  -p ：并同时列出每个 process 的 PID；
  -u ：并同时列出每个 process 的所属帐号名称。
  ```

- 利用`kill`和`killall`命令给程序发信号
  
  ```
  使用方式:
  killall -signal 指令名称  
  kill -signal PID
  讯号所代表的含义
  -1  : 启动被终止的程序，可让该 PID 重新读取自己的配置文件，类似重新启动
  -9  : 强制中断一个程序的进行，
  -15 : 以正常的结束程序来终止该程序
  ```

- `uname -a`查阅系统与核心相关信息

- `fuser`通过这个命令我们可以找出使用该文件、目录的程序，借以观察

- `systemctl`命令的其它用法
  
  ```
  systemctl enable nginx 设置nginx为开机自启服务
  systemctl disable nginx 关闭nginx开机自启服务
  systemctl is-enabled nginx 查询nginx是否为开机自启服务 
  ```

- `Linux`运行日志, 在`/var/log`目录下保存着`Linux`各项服务的运行日志
  
  ```
  /var/log/boot.log： 开机日志, 不过这个文件只会存这次开机启动的信息，前次开机的信息并不会被保留下来！
  /var/log/cron : 定时任务日志
  /var/log/messages :乎系统发生的错误讯息（或者是重要的信息）都会记录在这个文件中, 发生系统错误时应该查看这个
  ```

- `/proc`目录是一个提供内核统计信息的文件系统接口, `/proc`包含很多目录, 其中以进程ID命名的目录代表的就是那个进程,这些目录下包含进程信息和统计数据

- `strace`命令的使用
  
  ```
  strace -ttt -T -p 31614
  -ttt : 打印第一栏UNIX时间戳, 以秒为单位, 精确度可以到毫秒级
  -T     : 输出最后一栏, 这是系统调用时间, 以秒为单位, 精确到毫秒级
  -P   : 跟踪这个PID的进程
  查找配置文件:
  strace php 2>&1 | grep php.ini 
  统计调用次数和花费时间
  strace -c -p PID
  跟踪命令
  strace Command
  跟踪指定的系统调用
  strace -e open -p PID
  ```

- 通过yum命令安装php扩展包
  
  ```
  sudo yum install php74-php-pecl-zip.x86_64
  sudo cp /opt/remi/php74/root/usr/lib64/php/modules/zip.so /usr/lib64/php/modules/zip.so
  sudo nano /etc/php.d/40-zip.ini
  extension=zip.so
  ```
