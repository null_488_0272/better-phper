- wireshark抓包常见提示含义解析

  | Packet size limited during capture    | 说明被标记的那个包没有抓全 |
  | ------------------------------------- | -------------------------- |
  | **TCP Previous segment not captured** |                            |
  |                                       |                            |
  |                                       |                            |
  |                                       |                            |
  |                                       |                            |

  