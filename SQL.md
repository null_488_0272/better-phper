#### SQL技巧及知识点

------

- ```
  模糊匹配关键字`LIKE`除了有百分号`（%）`通配符外还有下划线`（____）`通配符,下划线通配符可以用来匹配字符个数
  WHERE name LIKE '杨_' //这条语句表示检索姓杨,名字有两个字的记录,下划线的个数代表需要匹配的字符个数
  ```
  
  ```
  在`sql`中`AND`比`OR`优先级要高, 在使用时要注意
  WHERE product_id = 1 OR product_id = 3 AND price > 100 //这条语句会被解释成,查询商品ID为1并且价格大于100的记录,以及商品ID为3的记录,
  这是因为AND操作符的优先级比OR高造成的
  ```

- ```
  使用`conect()`, `group_conect()`等函数可以实现字段拼接
  ```

- ```
  在WHERE子句中引用别名列
  1. 问题
  你已经为检索结果集创建了有意义的列名，并且想利用 WHERE 子句过滤掉部分行数据。但是，如果你尝试在 WHERE 子句中引用别名列，查询无法顺利执行。
  select sal as salary, comm as commission from emp where salary < 5000
  2. 解决方案
  把查询包装为一个内嵌视图，这样就可以引用别名列了。
  select * from (select sal as salary, comm as commission from emp) x where salary < 5000
  3. 讨论
  在这个简单的实例中，你可以不使用内嵌视图。在 WHERE 子句里直接引用 COMM 列和 SAL列，也可以达到同样的效果。当你想在 WHERE 子句中引用下列内容时，这个解决方案告诉你该如何做。
  将含有别名列的查询放入内嵌视图，就可以在外层查询中引用别名列。为什么要这么做呢？ WHERE 子句会比 SELECT 子句先执行，就最初那个失败的查询例子而言，
  当 WHERE 子句被执行时，SALARY 和 COMMISSION 尚不存在。直到 WHERE 子句执行完毕，那些别名列才会生效。然而，FROM 子句会先于 WHERE 子句执行。
  如果把最初的那个查询放入一个 FROM 子句，
  ```

- 在`sql`中字段与字段之间可以进行运算
  
  ```
  SELECT income / user_count as avg_income FROM order
  ```

- `count(*)`与`count(col)`的区别,`count(*)`可以用于`NULL`,而`count(col)`要先排除`NULL`的行再进行统计,当执行 COUNT(*) 操作时，实际上是在统计行数（而不会去管实际的值是什么，这就是为什 么 Null 值和非 Null 值都会被计入总数）。但是，如果针对某一列执行 COUNT 操作，我们却是在计算该列非 Null 值的个数

- 关于列设置可为`NULL`的情况说明, 在`SQL`中聚合函数遇见为`NULL`的列时,会排除掉再进行统计,为`NULL`的列会从二值逻辑变化为三值逻辑,也就是当列为`NULL`时`SQL`的逻辑判断不再是`false`或者`true`,而是被当成`unknown`来处理,所以当列存在`NULL`值时, 需要注意其处理方式

- `ALL`运算符是一个逻辑运算符，它将单个值与子查询返回的单列值集进行比较
  
  ```
  SELECT * FROM table_name WHERE column_name > ALL (subquery) 查询查找column_name列中的值大于子查询返回的最大值的行
  ```

- 求众数,如下案例是我需要从评价表中找出被评价最多的客服人员
  
  ```
  SELECT star, user_id, count(*) AS cnt FROM rates GROUP BY star HAVING count(*) >= ALL (SELECT count(*) FROM rates GROUP BY star)
  ```

- `case`表达式的写法(  )
  
  ```
  简单case表达式 CASE sex WHEN '1' THEN '男' WHEN '2' THEN '女' ELSE '其它' END
  搜索case表达式 CASE WHEN sex='1' THEN '男' WHEN sex='2' THEN '女' ELSE '其它' END
  ```

- case表达式需要注意的点
  
  ```
  1.在发现为真的WHEN子句时, CASE表达式的真假判断就会中止
  2.case表达式里各个分支返回的数据类型需要一致,某个分支返回字符串,而其它分支返回数值型的写法是不正确的(在实际测试中不存在此问题,可能是mysql版本的问题)
  3.不要忘记写case表达式的结束子句end
  4.每个case表达式最好明确的写上else子句
  ```

- 在连接查询时两张表的结果集是一个笛卡尔集,并不是简单的左表结果集连上右表结果集,我们可以把两张待连接的表看成两个集合,在一对一和一对多关系的两个集合,在进行连接操作后行数不会( 异常 )增加, 这个概念在需要使用连接和聚合来解决问题时非常有用
  
  ```
  假设集合A={a, b}，集合B={0, 1, 2}，则两个集合的笛卡尔积为{(a, 0), (a, 1), (a, 2), (b, 0), (b, 1), (b, 2)}。
  ```

- 使用自连接删除重复行( `“自连接”(self join)，这个技巧常常被人们忽视，其实是有挺多妙用的` )
  
  ```
  DELETE FROM Products P1 WHERE id < (SELECT MAX(P2.id) FROM Products P2  WHERE P1.name = P2.name AND P1.price = P2.price)
  ```

- 求余额,通过下列`sql`实现图中的查询结果 
  
  ```
  SELECT date, amount,(SELECT sum(amount) FROM accounts A2 WHERE A2.date <= A1.date ) AS balance FROM accounts A1 ORDER BY A1.date
  ```

- `union` 和 `union all`的区别,前者会过滤掉重复行而后者不会, 集合运算为了排除重复行, 默认的会发生排序, 而加上`all`选项后,就不会再排序了, 所以性能会有提升

- 有如下会议表meetings求没有参加某次会议的人
  
  ```
  SELECT DISTINCT M1.meeting, M2.person FROM meetings M1 JOIN meetings M2 WHERE NOT EXISTS (SELECT * FROM meetings M3 WHERE M1.meeting = M3.meeting AND M2.person = M3.person)
  ```

- `in`子句的变种用法( 下列语句是查询`p1`, `p2`, `p3`, `p4`, `p5`, `p6`六个字段中值等于100的记录, 只要有一个字段的值为100记录就能匹配上 )
  
  ```
  SELECT * FROM table_name WHERE 100 IN (p1, p2, p3, p4, p5, p6)
  ```

- `count`的各种骚操作
  
  | 条件表达式                                | 用途                       |
  | ------------------------------------ | ------------------------ |
  | `count (distinct col) = count(col)`  | `col`列没有重复的值             |
  | `count(*) = count(col)`              | `col`列不存在`null`          |
  | `count(*) = max(col)`                | `col`列是连续的编号( 起始值是1 )    |
  | `count(*) = max(col) - min(col) + 1` | `col`列是连续的编号( 起始值是任意整数 ) |
  | `max(col) = min(col)`                | `col`都是相同值或者是`null`      |
  | `max(col) * min(col) > 0`            | `col`列全是正数或全是负数          |
  | `max(col) * min(col) < 0`            | `col`列的最大值是正数, 最小值是负数    |
  | `min(ABS(col)) = 0`                  | `col`最少有一个0              |
  | `min(col - 常量) = -max(col - 常量)`     | `col`列的最大值和最小值与指定常量等距    |

- ```
  随机返回若干行记录,把内置函数 RAND 和 LIMIT、ORDER BY 结合使用
  select name,job from emp order by rand() limit 5
  ```

- ```
  使用聚合函数时一定要记住，Null 值会被忽略
  把Null值转换为实际值, 使用 COALESCE 函数将 Null 值替代为实际值
  select coalesce(name,0) from emp
  需要为 COALESCE 函数指定一个或多个参数。该函数会返回参数列表里的第一个非 Null 值。在本例中，若 name 不为 Null，会返回 name 值，否则返回 0
  也可以使用case方式来实现
  select case when name is not null then name else 0 end from emp
  ```

- ```
  视图的使用
  创建: CREATE VIEW V AS SELECT a.title,a.user_id,c.content FROM articles a,comments c WHERE a.id = c.article_id
  使用: SELECT * FROM V
  ```

- ```
  关联子查询的使用
  查询有评论的文章:SELECT * FROM articles WHERE EXISTS (SELECT * FROM comments WHERE articles.id = comments.article_id)
  查询没评论的文章:SELECT * FROM articles WHERE NOT EXISTS (SELECT * FROM comments WHERE articles.id = comments.article_id)
  ```

- ```
  依据特定时间单位检索数据, 查询在2月和12月或者星期2入职的员工
  SELECT * FROM admin_user WHERE monthname(created_at) IN ('February', 'December') OR dayname(created_at) = 'Tuesday'
  ```

- ```
  使用WITH ROLLUP实现汇总功能, WITH ROLLUP的作用就是对分组运算后的结果进行再一次运算
  SELECT coalesce(name,'合计') name,sum(cost) cost FROM team_buildings GROUP BY name WITH ROLLUP
  ```

- 对数据进行分块并统计
  
  ```
  SELECT
      ceil(trx_id / 5.0) AS grp,
      min(trx_date) AS trx_start,
      max(trx_date) AS trx_end,
      sum(trx_cnt) AS total
  FROM
      trx_log
  GROUP BY
      ceil(trx_id / 5.0)
  ```

- 年月日加减法运算
  
  ```
  SELECT
      trx_id,
      trx_date - INTERVAL 5 DAY as trx_date,
      trx_cnt
  FROM
      trx_log
  ```

- 分组后取**topN**
  
  ```
  //使用子查询
  SELECT * from messages as M where id = (SELECT max(id) from messages where M.uuid = uuid) ORDER BY id
  //下面这种方法需要设置limit才会有效
  select * from (select  * from user order by time limit 10000)as t group by t.name
  //使用exists
  SELECT * from messages as M where not EXISTS (SELECT 1 from messages where M.id < id and M.uuid = uuid)
  ```

- 带有in的between操作符实现(下面的 SQL 语句选取alexa介于 1 和 20 之间但 country 不为 USA 和 IND 的所有行)
  
  ```
  select * from websites where(alexa between 1 and 20) and not country in('USA','IND')
  ```

- 字符串替换, 将字段definition中包含copy的替换成notifier
  
  ```
  update bp_process set definition = replace(definition,'copy','notifier') where id > 0
  ```

- 使用**count**实现分类去重计数
  
  ```
  SELECT
  	task_id,
  	count(DISTINCT IF(act_type='FinishedTask',uuid,NULL)) AS finish_users,
  	count(DISTINCT IF(act_type='HitAdvBtn',uuid,NULL)) AS hit_adv_users,
  	count(DISTINCT IF(act_type='BeforeWatchAdv',uuid,NULL)) AS watch_adv_users,
  	sum(act_type = 'HitAdvBtn') AS hit_adv_times
  FROM
  	`task_acts`
  WHERE
  	`date` BETWEEN '2023-05-05'
  AND '2023-05-26'
  GROUP BY
  	`task_id`
  LIMIT 10 OFFSET 0
  ```
  
  
